# Xenopus tropicalis 10.0 genome assembly files

This project houses gene models (GFF) and other metadata files for the X. tropicalis 10.0 genome assembly.

## File List

XENTR_10.0_Xenbase.gff3 - Gene models for X. tropicalis 10.0 genome assembly.

### Branches

Master - This branch holds the stable releases of the GFF file.

Name_metadata - This branch holds incremental changes to model names, dbxrefs, etc.

Structural - This branch holds incremental changes to gene model structures, including: adding, removing, and altering exons and transcripts, as well as adding/removing gene models.

### External Files

[Xenopus tropicalis 10.0 genome FASTA file](http://ftp.xenbase.org/pub/Genomics/JGI/Xentr10.0/XENTR_10.0_genome.fasta.gz)



